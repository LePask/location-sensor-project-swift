//
//  ViewController.swift
//  MoviesCoreData
//
//  Created by Pascual Garay.
//  Copyright © 2019 DCE. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var tableView: UITableView!
    var movies : [Movie] = []
    let locationManager: CLLocationManager = CLLocationManager()
    var actualLocation:String = ""
    
    @IBAction func addActualLocation(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let coordinate = Movie(context: context)
        coordinate.name = actualLocation
        // Save data to coredata
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        getData()
        // get the data from coredata
        getData()
        // reload the tableview
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
        // Code to get the current location -----
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        // --------------------------------------
        tableView.rowHeight = 80
    }
    
    func locationManager (_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        for currentLocation in locations{
            actualLocation = currentLocation.description
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // get the data from coredata
        getData()
        // reload the tableview
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel!.numberOfLines = 5;
        //cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
        let movie = movies[indexPath.row]
        cell.textLabel?.text = "\(movie.name!)"
        return cell
    }
    
    func getData(){
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do {
            movies = try context.fetch(Movie.fetchRequest())
        }catch{
            print("Fetching failed")
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        if editingStyle == .delete{
            let movie = movies[indexPath.row]
            context.delete(movie)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            do{
            movies = try context.fetch(Movie.fetchRequest())
            }catch{
                print("Fetching failed")
            }
        }
        tableView.reloadData()
    }
    
}

